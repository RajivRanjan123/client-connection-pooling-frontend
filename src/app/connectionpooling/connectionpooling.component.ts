import { Component, OnInit } from '@angular/core';
import { ClientConnectionService } from '../client-connection.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-connectionpooling',
  templateUrl: './connectionpooling.component.html',
  styleUrls: ['./connectionpooling.component.css']
})
export class ConnectionpoolingComponent implements OnInit {

  clientConnectionForm:FormGroup;

  constructor(private clientConnectionService:ClientConnectionService, private fb:FormBuilder) { 
    
    this.clientConnectionForm= fb.group({
      initialSize:['', Validators.required],
      maxWait:['', Validators.required],
      maxActive:['', Validators.required],
      maxIdle:['', Validators.required],
      minIdle:['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

  onSubmit(){
  //   let conn={
  //     initialSize:val.initialSize,
  // maxWait:val.maxWait,
  // maxActive:val.maxActive,
  // maxIdle:val.maxIdle,
  // minIdle:val.minIdle
  //   }
    // console.log(val);
    // this.clientConnectionService.postConnData(conn);
    
    // this.clientConnectionForm.controls['initialSize'].setValue(this.clientConnectionForm.value.initialSize);
    // this.clientConnectionForm.controls['maxWait'].setValue(this.clientConnectionForm.value.maxWait);
    // this.clientConnectionForm.controls['maxActive'].setValue(this.clientConnectionForm.value.maxActive);
    // this.clientConnectionForm.controls['maxIdle'].setValue(this.clientConnectionForm.value.maxIdle);
    // this.clientConnectionForm.controls['minIdle'].setValue(this.clientConnectionForm.value.minIdle);
    console.log(this.clientConnectionForm.value);
    this.clientConnectionService.postConnData(this.clientConnectionForm.value);

  }

}
