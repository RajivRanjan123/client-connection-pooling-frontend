import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionpoolingComponent } from './connectionpooling.component';

describe('ConnectionpoolingComponent', () => {
  let component: ConnectionpoolingComponent;
  let fixture: ComponentFixture<ConnectionpoolingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnectionpoolingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionpoolingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
