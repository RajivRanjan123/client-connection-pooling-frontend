import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnectionpoolingComponent } from './connectionpooling/connectionpooling.component';

const routes: Routes = [
  {
    path:"connecionpool",
    component:ConnectionpoolingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
